-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 29, 2013 at 11:29 PM
-- Server version: 5.5.32
-- PHP Version: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Sammy`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookmaker`
--

CREATE TABLE IF NOT EXISTS `bookmaker` (
  `bookmaker_id` int(11) NOT NULL AUTO_INCREMENT,
  `bookmaker_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `bookmaker_xml_id` int(11) NOT NULL,
  PRIMARY KEY (`bookmaker_id`),
  UNIQUE KEY `bookmaker_xml_id` (`bookmaker_xml_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `country_xml_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_xml_id` (`country_xml_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fixture`
--

CREATE TABLE IF NOT EXISTS `fixture` (
  `fixture_id` int(11) NOT NULL AUTO_INCREMENT,
  `fixture_xml_id` int(11) NOT NULL,
  `fixture_host_id` int(11) NOT NULL,
  `fixture_xml_host_id` int(11) NOT NULL,
  `fixture_guest_id` int(11) NOT NULL,
  `fixture_xml_guest_id` int(11) NOT NULL,
  `fixture_date` date NOT NULL,
  `fixture_hour` time NOT NULL,
  `fixture_half_time__home_score` int(11) NOT NULL,
  `fixture_half_time__away_score` int(11) NOT NULL,
  `fixture_home_score` int(11) NOT NULL,
  `fixture_away_score` int(11) NOT NULL,
  `fixture_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `league_id` int(11) NOT NULL,
  PRIMARY KEY (`fixture_id`),
  UNIQUE KEY `fixture_xml_id` (`fixture_xml_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `league`
--

CREATE TABLE IF NOT EXISTS `league` (
  `league_id` int(11) NOT NULL AUTO_INCREMENT,
  `league_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `league_country_id` int(11) NOT NULL,
  `league_xml_champ_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`league_id`),
  UNIQUE KEY `league_xml_champ_id` (`league_xml_champ_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `market`
--

CREATE TABLE IF NOT EXISTS `market` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `market_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`market_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `odds`
--

CREATE TABLE IF NOT EXISTS `odds` (
  `odds_id` int(11) NOT NULL AUTO_INCREMENT,
  `odds_fixture_id` int(11) NOT NULL,
  `odds_decimal` decimal(5,2) NOT NULL,
  `odds_bookmaker_id` int(11) NOT NULL,
  `odds_market_id` int(11) NOT NULL,
  PRIMARY KEY (`odds_id`),
  UNIQUE KEY `odds_fixture_id` (`odds_fixture_id`,`odds_bookmaker_id`,`odds_market_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `team_xml_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`team_id`),
  UNIQUE KEY `team_xml_id` (`team_xml_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
