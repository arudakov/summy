-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 26, 2013 at 09:39 PM
-- Server version: 5.5.31
-- PHP Version: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Sammy`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookmaker`
--

CREATE TABLE IF NOT EXISTS `bookmaker` (
  `bookmaker_id` int(11) NOT NULL AUTO_INCREMENT,
  `bookmaker_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `bookmaker_xml_id` int(11) NOT NULL,
  PRIMARY KEY (`bookmaker_id`),
  UNIQUE KEY `bookmaker_name` (`bookmaker_name`,`bookmaker_xml_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `bookmaker`
--

INSERT INTO `bookmaker` (`bookmaker_id`, `bookmaker_name`, `bookmaker_xml_id`) VALUES
(3, '10Bet', 14),
(4, '188Bet', 56),
(5, '5 Dimes', 20);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `country_xml_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_xml_id` (`country_xml_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `country_xml_id`, `date_added`) VALUES
(10, 'International', 13, '2013-07-24 15:36:51');

-- --------------------------------------------------------

--
-- Table structure for table `fixture`
--

CREATE TABLE IF NOT EXISTS `fixture` (
  `fixture_id` int(11) NOT NULL AUTO_INCREMENT,
  `fixture_xml_id` int(11) NOT NULL,
  `fixture_host_id` int(11) NOT NULL,
  `fixture_xml_host_id` int(11) NOT NULL,
  `fixture_guest_id` int(11) NOT NULL,
  `fixture_xml_guest_id` int(11) NOT NULL,
  `fixture_date` date NOT NULL,
  `fixture_hour` time NOT NULL,
  `fixture_half_time__home_score` int(11) NOT NULL,
  `fixture_half_time__away_score` int(11) NOT NULL,
  `fixture_home_score` int(11) NOT NULL,
  `fixture_away_score` int(11) NOT NULL,
  `fixture_status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `league_id` int(11) NOT NULL,
  PRIMARY KEY (`fixture_id`),
  UNIQUE KEY `fixture_xml_id` (`fixture_xml_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=210 ;

--
-- Dumping data for table `fixture`
--

INSERT INTO `fixture` (`fixture_id`, `fixture_xml_id`, `fixture_host_id`, `fixture_xml_host_id`, `fixture_guest_id`, `fixture_xml_guest_id`, `fixture_date`, `fixture_hour`, `fixture_half_time__home_score`, `fixture_half_time__away_score`, `fixture_home_score`, `fixture_away_score`, `fixture_status`, `league_id`) VALUES
(169, 5577344, 246, 25279, 247, 15972, '2013-07-25', '03:50:00', 0, 0, 0, 0, 'notstarted', 1),
(170, 5521600, 248, 528341, 249, 24763, '2013-07-25', '18:00:00', 0, 0, 0, 0, 'notstarted', 2),
(171, 5521610, 250, 15779, 251, 24484, '2013-07-25', '18:00:00', 0, 0, 0, 0, 'notstarted', 2),
(172, 5521560, 252, 4551, 253, 18770, '2013-07-25', '18:30:00', 0, 0, 0, 0, 'notstarted', 2),
(173, 5521622, 254, 18773, 255, 18694, '2013-07-25', '18:30:00', 0, 0, 0, 0, 'notstarted', 2),
(174, 5521603, 256, 14845, 257, 18539, '2013-07-25', '18:30:00', 0, 0, 0, 0, 'notstarted', 2),
(175, 5521534, 258, 402753, 259, 24891, '2013-07-25', '18:45:00', 0, 0, 0, 0, 'notstarted', 2),
(176, 5521567, 260, 5306, 261, 21879, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(177, 5521623, 262, 24675, 263, 21453, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(178, 5521462, 264, 18761, 265, 24956, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(179, 5521464, 266, 15369, 267, 24950, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(180, 5521537, 268, 5313, 269, 21899, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(181, 5521505, 270, 18739, 271, 18640, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(182, 5521573, 272, 15550, 273, 12249, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(183, 5521564, 274, 21992, 275, 425653, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(184, 5521627, 276, 158280, 277, 5372, '2013-07-25', '19:00:00', 0, 0, 0, 0, 'notstarted', 2),
(185, 5521434, 278, 21593, 279, 25246, '2013-07-25', '20:00:00', 0, 0, 0, 0, 'notstarted', 2),
(186, 5521520, 280, 22256, 281, 24946, '2013-07-25', '20:00:00', 0, 0, 0, 0, 'notstarted', 2),
(187, 5521545, 282, 25244, 283, 22241, '2013-07-25', '20:00:00', 0, 0, 0, 0, 'notstarted', 2),
(188, 5521542, 284, 18793, 285, 5814, '2013-07-25', '20:00:00', 0, 0, 0, 0, 'notstarted', 2),
(189, 5521574, 286, 24944, 287, 24855, '2013-07-25', '20:00:00', 0, 0, 0, 0, 'notstarted', 2),
(190, 5521632, 288, 18714, 289, 15382, '2013-07-25', '20:00:00', 0, 0, 0, 0, 'notstarted', 2),
(191, 5521607, 290, 11974, 291, 21971, '2013-07-25', '20:15:00', 0, 0, 0, 0, 'notstarted', 2),
(192, 5521620, 292, 14841, 293, 18886, '2013-07-25', '20:30:00', 0, 0, 0, 0, 'notstarted', 2),
(193, 5521616, 294, 24862, 295, 24812, '2013-07-25', '20:30:00', 0, 0, 0, 0, 'notstarted', 2),
(194, 5521635, 296, 24503, 297, 18656, '2013-07-25', '20:30:00', 0, 0, 0, 0, 'notstarted', 2),
(195, 5521570, 298, 12311, 299, 558672, '2013-07-25', '21:00:00', 0, 0, 0, 0, 'notstarted', 2),
(196, 5521571, 300, 401870, 301, 24901, '2013-07-25', '21:00:00', 0, 0, 0, 0, 'notstarted', 2),
(197, 5521612, 302, 527673, 303, 24751, '2013-07-25', '21:00:00', 0, 0, 0, 0, 'notstarted', 2),
(198, 5521474, 304, 21665, 305, 5490, '2013-07-25', '21:15:00', 0, 0, 0, 0, 'notstarted', 2),
(199, 5521577, 306, 21392, 307, 21996, '2013-07-25', '21:30:00', 0, 0, 0, 0, 'notstarted', 2),
(200, 5521601, 308, 18382, 309, 15645, '2013-07-25', '21:30:00', 0, 0, 0, 0, 'notstarted', 2),
(201, 5521566, 310, 21500, 311, 19003, '2013-07-25', '21:30:00', 0, 0, 0, 0, 'notstarted', 2),
(202, 5521614, 312, 24640, 313, 18941, '2013-07-25', '21:30:00', 0, 0, 0, 0, 'notstarted', 2),
(203, 5521634, 314, 24839, 315, 5992, '2013-07-25', '21:30:00', 0, 0, 0, 0, 'notstarted', 2),
(204, 5521563, 316, 25252, 317, 25014, '2013-07-25', '21:45:00', 0, 0, 0, 0, 'notstarted', 2),
(205, 5521521, 318, 18682, 319, 21979, '2013-07-25', '21:45:00', 0, 0, 0, 0, 'notstarted', 2),
(206, 5521576, 320, 5445, 321, 15954, '2013-07-25', '21:45:00', 0, 0, 0, 0, 'notstarted', 2),
(207, 5521624, 322, 21662, 323, 21585, '2013-07-25', '21:45:00', 0, 0, 0, 0, 'notstarted', 2),
(208, 5521625, 324, 21461, 325, 24269, '2013-07-25', '21:45:00', 0, 0, 0, 0, 'notstarted', 2),
(209, 5521446, 326, 24810, 327, 1064766, '2013-07-25', '22:00:00', 0, 0, 0, 0, 'notstarted', 2);

-- --------------------------------------------------------

--
-- Table structure for table `league`
--

CREATE TABLE IF NOT EXISTS `league` (
  `league_id` int(11) NOT NULL AUTO_INCREMENT,
  `league_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `league_country_id` int(11) NOT NULL,
  `league_xml_champ_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`league_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `league`
--

INSERT INTO `league` (`league_id`, `league_name`, `league_country_id`, `league_xml_champ_id`, `date_added`) VALUES
(1, 'Copa Libertadores Playoff', 10, 3127710, '2013-07-24 17:09:48'),
(2, 'Europa League', 10, 3131015, '2013-07-24 17:09:48');

-- --------------------------------------------------------

--
-- Table structure for table `market`
--

CREATE TABLE IF NOT EXISTS `market` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `market_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`market_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `market`
--

INSERT INTO `market` (`market_id`, `market_type`) VALUES
(1, 'home'),
(2, 'draw'),
(3, 'away'),
(4, 'under_2.0'),
(5, 'under_2.5'),
(6, 'under_3.0'),
(7, 'over_2.5'),
(8, 'over_2.0'),
(9, 'over_3.0'),
(10, 'under_2.25'),
(11, 'over_2.25'),
(12, 'under_2.75'),
(13, 'over_2.75'),
(14, 'HT_home'),
(15, 'HT_draw'),
(16, 'HT_away'),
(17, 'HT_under_0.75'),
(18, 'HT_under_1.0'),
(19, 'HT_over_0.75'),
(20, 'HT_over_1.0');

-- --------------------------------------------------------

--
-- Table structure for table `odds`
--

CREATE TABLE IF NOT EXISTS `odds` (
  `odds_id` int(11) NOT NULL AUTO_INCREMENT,
  `odds_fixture_id` int(11) NOT NULL,
  `odds_decimal` decimal(10,0) NOT NULL,
  `odds_bookmaker_id` int(11) NOT NULL,
  `odds_market_id` int(11) NOT NULL,
  PRIMARY KEY (`odds_id`),
  UNIQUE KEY `odds_fixture_id` (`odds_fixture_id`,`odds_bookmaker_id`,`odds_market_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `odds`
--

INSERT INTO `odds` (`odds_id`, `odds_fixture_id`, `odds_decimal`, `odds_bookmaker_id`, `odds_market_id`) VALUES
(1, 169, 1, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `team_xml_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`team_id`),
  UNIQUE KEY `team_name` (`team_name`,`team_xml_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=328 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`team_id`, `team_name`, `team_xml_id`, `date_added`) VALUES
(246, 'AtlÃ©tico MG', 25279, '2013-07-25 18:12:22'),
(247, 'Olimpia', 15972, '2013-07-25 18:12:22'),
(248, 'FC Milsami Orhei', 528341, '2013-07-25 18:12:22'),
(249, 'Shakhtyor Soligorsk', 24763, '2013-07-25 18:12:22'),
(250, 'Trencin', 15779, '2013-07-25 18:12:22'),
(251, 'IFK Gothenburg', 24484, '2013-07-25 18:12:22'),
(252, 'Rudar Pljevlja', 4551, '2013-07-25 18:12:22'),
(253, 'Slask', 18770, '2013-07-25 18:12:22'),
(254, 'Piast Gliwice', 18773, '2013-07-25 18:12:22'),
(255, 'FK Karabakh', 18694, '2013-07-25 18:12:22'),
(256, 'Zilina', 14845, '2013-07-25 18:12:22'),
(257, 'Ol Ljubljana', 18539, '2013-07-25 18:12:22'),
(258, 'Turnovo', 402753, '2013-07-25 18:12:22'),
(259, 'Hajduk Split', 24891, '2013-07-25 18:12:22'),
(260, 'Inter Baku', 5306, '2013-07-25 18:12:22'),
(261, 'TromsÃ¸', 21879, '2013-07-25 18:12:22'),
(262, 'Sturm Graz', 24675, '2013-07-25 18:12:22'),
(263, 'Breidablik', 21453, '2013-07-25 18:12:22'),
(264, 'Chikhura', 18761, '2013-07-25 18:12:22'),
(265, 'Thun', 24956, '2013-07-25 18:12:22'),
(266, 'Khazar Lenkoran', 15369, '2013-07-25 18:12:22'),
(267, 'Maccabi Haifa', 24950, '2013-07-25 18:12:22'),
(268, 'Jeunesse Esch', 5313, '2013-07-25 18:12:22'),
(269, 'Ventspils', 21899, '2013-07-25 18:12:22'),
(270, 'FC Aktobe', 18739, '2013-07-25 18:12:23'),
(271, 'HÃ¸dd', 18640, '2013-07-25 18:12:23'),
(272, 'FC Pyunik', 15550, '2013-07-25 18:12:23'),
(273, 'Zalgiris Vilnius', 12249, '2013-07-25 18:12:23'),
(274, 'Rubin Kazan', 21992, '2013-07-25 18:12:23'),
(275, 'Jagodina', 425653, '2013-07-25 18:12:23'),
(276, 'Minsk FC', 158280, '2013-07-25 18:12:23'),
(277, 'Valletta', 5372, '2013-07-25 18:12:23'),
(278, 'BK HÃ¤cken', 21593, '2013-07-25 18:12:23'),
(279, 'Sparta Prague', 25246, '2013-07-25 18:12:23'),
(280, 'Pandurii', 22256, '2013-07-25 18:12:23'),
(281, 'FC Levadia Tallinn', 24946, '2013-07-25 18:12:23'),
(282, 'Liberec', 25244, '2013-07-25 18:12:23'),
(283, 'FC Skonto Riga', 22241, '2013-07-25 18:12:23'),
(284, 'Omonia Nicosia', 18793, '2013-07-25 18:12:23'),
(285, 'Astra Ploiesti', 5814, '2013-07-25 18:12:23'),
(286, 'Hapoel Tel Aviv', 24944, '2013-07-25 18:12:23'),
(287, 'Beroe', 24855, '2013-07-25 18:12:23'),
(288, 'Gefle', 18714, '2013-07-25 18:12:23'),
(289, 'Anorthosis', 15382, '2013-07-25 18:12:23'),
(290, 'Dacia Chisinau', 11974, '2013-07-25 18:12:23'),
(291, 'Chernomorets Odesa', 21971, '2013-07-25 18:12:23'),
(292, 'FK Senica', 14841, '2013-07-25 18:12:23'),
(293, 'Mladost Podgorica', 18886, '2013-07-25 18:12:23'),
(294, 'Botev Plovdiv', 24862, '2013-07-25 18:12:23'),
(295, 'Å½rinjski Mostar', 24812, '2013-07-25 18:12:23'),
(296, 'Utrecht', 24503, '2013-07-25 18:12:24'),
(297, 'Differdang03', 18656, '2013-07-25 18:12:24'),
(298, 'VÃ­kingur GÃ¸ta', 12311, '2013-07-25 18:12:24'),
(299, 'Petrolul', 558672, '2013-07-25 18:12:24'),
(300, 'Prestatyn Town', 401870, '2013-07-25 18:12:24'),
(301, 'Rijeka', 24901, '2013-07-25 18:12:24'),
(302, 'NK Lokomotiva', 527673, '2013-07-25 18:12:24'),
(303, 'Dinamo Minsk', 24751, '2013-07-25 18:12:24'),
(304, 'Aalborg', 21665, '2013-07-25 18:12:24'),
(305, 'Dila Gori', 5490, '2013-07-25 18:12:24'),
(306, 'Vestmannaeyar', 21392, '2013-07-25 18:12:24'),
(307, 'Crvena Zvezda', 21996, '2013-07-25 18:12:24'),
(308, 'Budapest Honved', 18382, '2013-07-25 18:12:24'),
(309, 'Vojvodina', 15645, '2013-07-25 18:12:24'),
(310, 'Debrecen FC', 21500, '2013-07-25 18:12:24'),
(311, 'StrÃ¸msgodset', 19003, '2013-07-25 18:12:24'),
(312, 'Standard Liege', 24640, '2013-07-25 18:12:24'),
(313, 'Reykjavik', 18941, '2013-07-25 18:12:24'),
(314, 'Å iroki Brijeg', 24839, '2013-07-25 18:12:24'),
(315, 'Irtysh Pavlodar', 5992, '2013-07-25 18:12:24'),
(316, 'Hibernian', 25252, '2013-07-25 18:12:24'),
(317, 'Malmo FF', 25014, '2013-07-25 18:12:24'),
(318, 'Linfield', 18682, '2013-07-25 18:12:25'),
(319, 'Xanthi FC', 21979, '2013-07-25 18:12:25'),
(320, 'Lech PoznaÅ„', 5445, '2013-07-25 18:12:25'),
(321, 'Honka', 15954, '2013-07-25 18:12:25'),
(322, 'Saint Johnstone', 21662, '2013-07-25 18:12:25'),
(323, 'Rosenborg', 21585, '2013-07-25 18:12:25'),
(324, 'Derry', 21461, '2013-07-25 18:12:25'),
(325, 'Trabzonspor', 24269, '2013-07-25 18:12:25'),
(326, 'FK Sarajevo', 24810, '2013-07-25 18:12:25'),
(327, 'Kukesi', 1064766, '2013-07-25 18:12:25');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
