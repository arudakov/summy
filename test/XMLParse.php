<?php
/*
 * Author: Rudakov Aleksey
 * Skype: tra_tra_al
 * Email:rudakovalal@gmail.com
 * Odesk profile:https://www.odesk.com/users/~0131ebdb4c21e9dcfb
 * 
 */

/*
 * XMLParse class implements methods for parsing xml-file with specified hierarchy and 
 * filing database with results of parsing. Database should have special architecture for 
 * XMLParse class working correctly.
 * 
 * To use this class in your project,just include this php file in your sources. 
 * To parse xml-file just create object of this class with special inputs:
 


 *  @param $xmlPath string: relative path to local xml file or url to remote xml.
 *  Local files are prefered, becouse they are processed much faster(up to few seconds). Remote files can be processed 
 *  during several minutes !
 * 
 *  @param $db mixed: can be instance of mysqli class created outside or array of
 *  database params needed for database connection.
 * 
 * @param $returnArrays boolean, default is true. If it is FALSE, script will fill database 
 * without returning array of parsed data. 
 *  
 * 
 * While script is working, it create object and fills database and objects properties with parsed data.
 * 
 * If something is going wrong during script executing, parsed data will not be saved in database at all 
 * and process will be rolled back.  Property error will contain error message.
 * 
 */





class XMLParse{

        /*
         * These properties will containg parsed data after parsing process will be 
         * completed and object of XMLParse class will be created.
         * NOTE! It will contain only data, which wasn't in database before script working and was saved .
         */
        public $leagues = array();
        public $countries = array();
        public $teams = array();
        public $fixtures = array();
        public $odds = array();
        public $bookmakers = array();
        public $markets = array();
        
        public $error; // is error message in case something is going wrong, otherwise is 0
        
        private $_mysqli;
        
        
        public function __construct($pathXml,$db,$returnArrays = true){ 
                
                $this->error = 0;
                
                $this->_mysqli = $this->_connectDB($db);
                $xml = simplexml_load_file($pathXml);
                if (!$xml)
                        $this->error = "Failed to prepare XML ".$pathXml;
                if ($this->_mysqli && $xml)
                {
                        
                        $json = json_encode($xml);
                        $arrjson = json_decode($json,TRUE);
                        
                        $this->_mysqli->autocommit(false);
                        
                        $maininfo = $arrjson['maininfo'];
                        
                        $res = $this->xmlToArr($maininfo,$returnArrays);      
                        if($res) 
                                $this->_mysqli->commit();
                        else 
                        {
                                $this->_mysqli->rollback();
                                $this->leagues = array();
                                $this->countries = array();
                                $this->teams = array();
                                $this->fixtures = array();
                                $this->odds = array();
                                $this->bookmakers = array();
                                $this->markets = array();
                        }
                }
        }
        
        
        /*
         * Main function of class. It realize process of parsing, saving in db and 
         * filling class properties whis parsed data. It gets xml nodes one by one and 
         * if there is no such record in database it will save it, other wise it wll just ignore it.
         * Return 1 if process completed successfuly and 0 if there are some errors.
         * 
         * 
         */
        
        
        private function xmlToArr($maininfo,$returnArrays)
        {
                 foreach($maininfo as $leagueXML){
                
                        $league_id = $this->getIdByAttr('league','league_xml_champ_id',$leagueXML['champ_id']);
                        if($league_id<0) return 0;
                        if (!$league_id)
                        {
                                $league["league_xml_champ_id"] = $leagueXML['champ_id'];
                                $league["league_name"] = $leagueXML["champname"];

                                $country_id = $this->getIdByAttr('country','country_xml_id',$leagueXML['country_id']);
                                if($country_id<0) return 0;
                                
                                if($country_id)
                                        $league["league_country_id"] = $country_id;
                                else 
                                {
                                        $country['country_name'] = $leagueXML['country'];
                                        $country['country_xml_id'] = $leagueXML['country_id'];
                                        $country_id = $this->insertRow('country',$country);
                                        if($country_id<0) return 0;
                                        
                                        if ($returnArrays)
                                                $this->countries[] = $this->getRow('country', $country_id);
                                        
                                        $league["league_country_id"] = $country_id;
                                }

                                $league_id = $this->insertRow('league', $league);
                                if($league_id<0) return 0;
                                if ($returnArrays)
                                        $this->leagues[] = $this->getRow('league', $league_id);
                        }

                        foreach ($leagueXML['games'] as $fixtureXML)
                        {
                               $fixture_id = $this->getIdByAttr('fixture','fixture_xml_id',$fixtureXML['id']); 
                               if($fixture_id<0) return 0;
                               
                               if(!$fixture_id){
                               
                                       $fixture['fixture_xml_id'] = $fixtureXML['id'];

                                       $fixture['fixture_date'] = $fixtureXML['date'];

                                       $fixture['fixture_hour'] = $fixtureXML['hour'];

                                       if (isset($fixtureXML['half_time__home_score']))
                                                $fixture['fixture_half_time__home_score'] = $this->toZero($fixtureXML['half_time__home_score']);

                                       if (isset($fixtureXML['half_time__away_score']))
                                                $fixture['fixture_half_time__away_score'] = $this->toZero($fixtureXML['half_time__away_score']);

                                       if (isset($fixtureXML['fixture_home_score']))
                                                $fixture['fixture_home_score'] = $this->toZero($fixtureXML['fixture_home_score']);

                                       if (isset($fixtureXML['fixture_away_score']))
                                                $fixture['fixture_away_score'] = $this->toZero($fixtureXML['fixture_away_score']);

                                       $fixture['fixture_status'] = $fixtureXML['status_type'];

                                       $fixture['league_id'] = $league_id;


                                       $host_id = $this->getIdByAttr('team','team_xml_id',$fixtureXML['host_id']); 
                                       
                                       if($host_id<0) return 0;
                                       
                                       if ($host_id)
                                       {
                                               $fixture['fixture_host_id'] = $host_id;
                                               $fixture['fixture_xml_host_id'] = $fixtureXML['host_id'] ;

                                       }else 
                                       {
                                               $host['team_xml_id'] = $fixtureXML['host_id'];
                                               $host['team_name'] = $fixtureXML['host'];

                                               $host_id = $this->insertRow('team', $host);
                                               if($host_id<0) return 0;
                                               if ($returnArrays) 
                                                       $this->teams[] = $this->getRow('team', $host_id);
                                               
                                               $fixture['fixture_xml_host_id'] = $fixtureXML['host_id'];
                                               $fixture['fixture_host_id'] = $host_id;
                                       }

                                       $guest_id = $this->getIdByAttr('team','team_xml_id',$fixtureXML['guest_id']); 
                                       if($guest_id<0) return 0;
                                       
                                       if ($guest_id)
                                       {
                                               $fixture['fixture_guest_id'] = $guest_id;
                                               $fixture['fixture_xml_guest_id'] = $fixtureXML['guest_id'];
                                       }else 
                                       {
                                               $guest['team_xml_id'] = $fixtureXML['guest_id'];
                                               $guest['team_name'] = $fixtureXML['guest'];
                                               
                                               $guest_id = $this->insertRow('team', $guest);
                                               if($guest_id<0) return 0;
                                               if ($returnArrays)
                                                        $this->teams[] = $this->getRow('team', $guest_id);

                                               $fixture['fixture_xml_guest_id'] = $fixtureXML['guest_id'];
                                               $fixture['fixture_guest_id'] = $guest_id;
                                       }

                                       $fixture_id = $this->insertRow('fixture',  $fixture);
                                       if ($fixture_id<0) return 0;
                                       
                                       $this->fixtures[] = $this->getRow('fixture', $fixture_id);
                               }


                               foreach ($fixtureXML["odds"] as $key=>$bookmakerXML)
                               {
                                       $bookXMLId = str_replace("Bookmaker_","", $key);
                                       $book_id = $this->getIdByAttr('bookmaker','bookmaker_xml_id',$bookXMLId); 
                                       if($book_id<0) return 0;
                                       if(!$book_id)
                                       {
                                               $bookmaker['bookmaker_xml_id'] = $bookXMLId;
                                               $bookmaker['bookmaker_name'] = $bookmakerXML['bookmaker'];

                                               $book_id = $this->insertRow('bookmaker', $bookmaker);
                                               if($book_id<0) return 0;
                                               if ($returnArrays)
                                                        $this->bookmakers[] = $this->getRow('bookmaker', $book_id);
                                       }

                                       foreach ($bookmakerXML as $xmlKey=>$oddsXML)
                                       {
                                               if($xmlKey == "bookmaker")
                                                       continue;
                                               $market_id = $this->getIdByAttr('market', 'market_type', $xmlKey);
                                               if(!$market_id)
                                               {
                                                       $market['market_type'] = $xmlKey;
                                                       
                                                       $market_id = $this->insertRow('market', $market);
                                                       if($market_id<0) return 0;
                                                       if ($returnArrays)
                                                               $this->markets = $this->getRow('market', $market_id);
                                               }
                                               
                                               $odd['odds_fixture_id'] = $fixture_id;
                                               $odd['odds_decimal'] = $oddsXML;
                                               $odd['odds_market_id'] = $market_id;
                                               $odd['odds_bookmaker_id'] = $book_id;
                                               
                                               $odd_id = $this->insertRow('odds', $odd);
                                               if($odd_id<0) return 0;
                                               
                                               if ($odd_id && $returnArrays)
                                                       $this->odds[] = $this->getRow('odds', $odd_id);
                                       }
                               }
                        }
                }
                return 1;
        }

        

        private function _connectDB($params){
                if ($params instanceof mysqli)
                        return $params;
                else 
                {
                        $mysqli = new mysqli($params['host'],$params['user'],$params['pass'],$params['db']);
                        if (mysqli_connect_errno())
                        {
                                $this->error = $mysqli->connect_error;
                                return 0;
                        }
                        $mysqli->set_charset('utf8');
                        return $mysqli;
                }
        }

        
        
        /*
         * Select row from table $table by id $id. 
         * Return associative array according to row structure  array
         */
        
        private function getRow($table,$id)
        {
                $sql = "SELECT * FROM $table WHERE `$table"."_id`=$id";
                $res = $this->_mysqli->query($sql);
                if (!$res) 
                {
                        $this->error = "Unable to execute:".$sql;
                        return -1;
                }
                if($res->num_rows ==0)
                        return 0;
                else 
                      return $res->fetch_assoc();
        }     

        
        /*
         * Inserts into table $table row specified in associative array $row
         * Returns the auto generated id used in the last query
         */
        
        private function insertRow($table,$row)
        {
                foreach ($row as $key=>$value)
                {
                        $colums[] = "`".  $this->_mysqli->real_escape_string($key)."`";
                        $values[] = "'".$this->_mysqli->real_escape_string($value)."'";
                }

                $sql = "INSERT INTO `$table` (".implode(",", $colums).") VALUES (".implode(",", $values).")";
                $res = $this->_mysqli->query($sql);

                if (!$res) 
                {
                        $code = $this->_mysqli->errno;
                        if ($code == 1062 && $table == "odds")
                                return 0;
                        else 
                        {
                                $this->error = "Unable to execute ".$sql;
                                return -1;
                        }
                }
                return $this->_mysqli->insert_id;
        }
        
        
        /*
         * Converts external xml id into auto generated internal id.
         * 
         * @param $table string: table name, where record with xml id is searched
         * @param $attr mixed: name of attribute, record is searched by. Should be unique.
         * @param $value mixed: value of attribute $attr.
         * 
         * Returns the auto generated id if  record with xml id is already existed in table $table
         * otherwise returns 0.
         */
        private function getIdByAttr($table,$attr,$value)
        {
                $sql = "SELECT `$table"."_id` FROM `$table` WHERE `$attr`='$value'";
                $res = $this->_mysqli->query($sql);
                if (!$res) 
                {
                        $this->error = "Unable to execute:".$sql;
                        return -1;
                }
                        
                if ($res->num_rows==0)
                        return 0;
                else 
                {
                        $a = $res->fetch_row();
                        return $a[0];
                }
        }
        
        
        private function toZero($str)
        {
                return (empty($str))?'0':$str;
        }
        
}

?>
