<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
</head>
<body>
<?php



$mysqli = new mysqli('localhost','root','','Sammy');
if($mysqli->connect_error) die('Connection failed: '.$mysqli->connect_error);

$xml = simplexml_load_file('http://devsgs.com/preview/parser/assets/data.xml');

$json = json_encode($xml);
$arrjson = json_decode($json,TRUE);

$maininfo = $arrjson['maininfo'];
$i = 0;
$ci = 0;


foreach($maininfo as $leagueXML)
{
        $league_id = getIdByAttr($mysqli, 'league','league_xml_champ_id',$leagueXML['champ_id']);
        if (!$league_id)
        {
                $leagues[$i]["league_xml_champ_id"] = $leagueXML['champ_id'];
                $leagues[$i]["league_name"] = $leagueXML["champname"];
                
                $country_id = getIdByAttr($mysqli, 'country','country_xml_id',$leagueXML['country_id']);
                if($country_id)
                        $leagues[$i]["league_country_id"] = $country_id;
                else 
                {
                        $countries[$ci]['country_name'] = $leagueXML['country'];
                        $countries[$ci]['country_xml_id'] = $leagueXML['country_id'];
                        insertRow($mysqli, 'country',$countries[$ci]);
                        $ci++;
                        $leagues[$i]["league_country_id"] = getIdByAttr($mysqli, 'country','country_xml_id', $leagueXML['country_id']);
                }
                
                insertRow($mysqli, 'league', $leagues[$i]);
                $league_id = getIdByAttr($mysqli, 'league','league_xml_champ_id',$leagueXML['champ_id']);
                $i++;
        }
        
        $j = 0;
        $ti = 0;
        foreach ($leagueXML['games'] as $fixtureXML)
        {
               $fixture_id = getIdByAttr($mysqli, 'fixture','fixture_xml_id',$fixtureXML['id']); 
               if(!$fixture_id)
               {
                       $fixtures[$j]['fixture_xml_id'] = $fixtureXML['id'];
                       
                       $fixtures[$j]['fixture_date'] = $fixtureXML['date'];
                       
                       $fixtures[$j]['fixture_hour'] = $fixtureXML['hour'];
                       
                       if (isset($fixtureXML['half_time__home_score']))
                                $fixtures[$j]['fixture_half_time__home_score'] = toZero($fixtureXML['half_time__home_score']);
                       
                       if (isset($fixtureXML['half_time__away_score']))
                                $fixtures[$j]['fixture_half_time__away_score'] = toZero($fixtureXML['half_time__away_score']);
                       
                       if (isset($fixtureXML['fixture_home_score']))
                                $fixtures[$j]['fixture_home_score'] = toZero($fixtureXML['fixture_home_score']);
                       
                       if (isset($fixtureXML['fixture_away_score']))
                                $fixtures[$j]['fixture_away_score'] = toZero($fixtureXML['fixture_away_score']);
                       
                       $fixtures[$j]['fixture_status'] = $fixtureXML['status_type'];
                       
                       $fixtures[$j]['league_id'] = $league_id;
                       
                       
                       $host_id = getIdByAttr($mysqli, 'team','team_xml_id',$fixtureXML['host_id']); 
                       if ($host_id)
                       {
                               $fixtures[$j]['fixture_host_id'] = $host_id;
                               $fixtures[$j]['fixture_xml_host_id'] = $fixtureXML['host_id'] ;
                               
                       }else 
                       {
                               $teams[$ti]['team_xml_id'] = $fixtureXML['host_id'];
                               $teams[$ti]['team_name'] = $fixtureXML['host'];
                              
                               insertRow($mysqli, 'team', $teams[$ti]);
                               $ti++;
                               
                               $fixtures[$j]['fixture_xml_host_id'] = $fixtureXML['host_id'];
                               $fixtures[$j]['fixture_host_id'] = getIdByAttr($mysqli, 'team','team_xml_id',$fixtureXML['host_id']);
                       }
                       
                       $guest_id = getIdByAttr($mysqli, 'team','team_xml_id',$fixtureXML['guest_id']); 
                       if ($guest_id)
                       {
                               $fixtures[$j]['fixture_guest_id'] = $guest_id;
                               $fixtures[$j]['fixture_xml_guest_id'] = $fixtureXML['guest_id'];
                       }else 
                       {
                               $teams[$ti]['team_xml_id'] = $fixtureXML['guest_id'];
                               $teams[$ti]['team_name'] = $fixtureXML['guest'];
                               insertRow($mysqli, 'team', $teams[$ti]);
                               $ti++;
                               
                               $fixtures[$j]['fixture_xml_guest_id'] = $fixtureXML['guest_id'];
                               $fixtures[$j]['fixture_guest_id'] = getIdByAttr($mysqli, 'team','team_xml_id',$fixtureXML['guest_id']);
                       }
                       
                       insertRow($mysqli, 'fixture',  $fixtures[$j]);
                       
                       $fixture_id = getIdByAttr($mysqli, 'fixture','fixture_xml_id',$fixtureXML['id']); 
                       $j++;
               }
               
               
               $k = 0;
               $bi = 0;
               foreach ($fixtureXML["odds"] as $key=>$bookmakerXML)
               {
                       $bookXMLId = str_replace("Bookmaker_","", $key);
                       $book_id = getIdByAttr($mysqli, 'bookmaker','bookmaker_xml_id',$bookXMLId); 
                       if(!$book_id)
                       {
                               $bookmaker['bookmaker_xml_id'] = $bookXMLId;
                               $bookmaker['bookmaker_name'] = $bookmakerXML['bookmaker'];
                              
                               $book_id = insertRow($mysqli, 'bookmaker', $bookmaker);
                               $bookmakers[] = getRow($mysqli, 'bookmaker', $book_id);
                       }
                       
                       foreach ($bookmakerXML as $xmlKey=>$oddsXML)
                       {
                               if($xmlKey == "bookmaker")
                                       continue;
                               $market_id = getIdByAttr($mysqli, 'market', 'market_type', $xmlKey);
                               if(!$market_id)
                               {
                                       $market['market_type'] = $xmlKey;
                                       $market_id = insertRow($mysqli, 'market', $market);
                               }
                               $odd['odds_fixture_id'] = $fixture_id;
                               $odd['odds_decimal'] = $oddsXML;
                               $odd['odds_market_id'] = $market_id;
                               $odd['odds_bookmaker_id'] = $book_id;
                               $odd_id = insertRow($mysqli, 'odds', $odd);
                               if ($odd_id)
                                       $odds[] = getRow($mysqli, 'odds', $odd_id);
                       }
               }
        }
}


function toZero($str)
{
        return (empty($str))?'0':$str;
}


function getIdByAttr($mysqli,$table,$attr,$value)
{
        $sql = "SELECT `$table"."_id` FROM `$table` WHERE `$attr`='$value'";
        $res = $mysqli->query($sql);
        if ($res == 0) 
                die (" Не удалось выполнить".$sql);
        if ($res->num_rows==0)
                return 0;
        else 
        {
                $a = $res->fetch_row();
                return $a[0];
        }
}


function getRow($mysqli,$table,$id)
{
        $sql = "SELECT * FROM $table WHERE `$table"."_id`=$id";
        $res = $mysqli->query($sql); 
        if($res->num_rows ==0)
                return 0;
        else 
              return $res->fetch_assoc();
}

function insertRow($mysqli,$table,$row)
{
        foreach ($row as $key=>$value)
        {
                $colums[] = "`".$key."`";
                $values[] = "'".$value."'";
        }
        
        $sql = "INSERT INTO `$table` (".implode(",", $colums).") VALUES (".implode(",", $values).")";
        $res = $mysqli->query($sql);
        
        if (!$res) 
        {
                $code = $mysqli->errno;
                if ($code == 1062)
                        return 0;
        }
        
        return $mysqli->insert_id;
}

?>
</body>
</html>
