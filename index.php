<!--
Example of using XMLParse class!

 Author: Rudakov Aleksey
 Skype: tra_tra_al
 Email:rudakovalal@gmail.com
 Odesk profile:https://www.odesk.com/users/~0131ebdb4c21e9dcfb


-->



<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
</head>
<body>
        
        <form method="POST">
                <label>Input relative path to local xml-file or URL:</label><br>
                <input type="text" name="xml-file"/>
                <input type="submit" value="go">
        </form>
        
        
<?php
        
        require_once 'XMLParse.php'; // include class file in project
        
        if(isset($_POST['xml-file']))
        {
                /*
                 * specify database params for database connection. 
                 * Array of database params sould have same indexes.
                 * 
                 * Also mysqli object created before can be used as well
                 * f.e. 
                 * $mysqli = new mysqli(params)...
                 * Then just put $mysqli in input of XMLParse object ctreating.
                 */
                
                $db = array(
                        'host'=>'localhost',
                        'user'=>'root',
                        'pass'=>'',
                        'db'=>'Sammy'
                );


                $start = microtime(true);

                /*
                 * Create XMLParse object to parse xml-file,save it to dababase and 
                 * return several array of parsed data.
                 */
                
                $xmlArrs = new XMLParse($_POST['xml-file'],$db); 

                $time = microtime(true) - $start;

                echo "<br><br>Script was perfomed in: ".$time." seconds<br>";

                if(!$xmlArrs->error){// check if there are no errors during script executing
                        echo "<br><br>Script run successfuly. Were saved in db:<br>";
                        echo "Leagues:".count($xmlArrs->leagues)."<br>"; // use arrays with parsed in project
                        echo "Countries:".count($xmlArrs->countries)."<br>";
                        echo "Fixtures:".count($xmlArrs->fixtures)."<br>";
                        echo "Teams:".count($xmlArrs->teams)."<br>";
                        echo "Bookmakers:".count($xmlArrs->bookmakers)."<br>";
                        echo "Odds:".count($xmlArrs->odds)."<br>";
                }
                else 
                       echo "Error: ".$xmlArrs->error;//output error was occured during script executing
        }
?>
        
</body>
</html>
